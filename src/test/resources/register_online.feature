# language : pt

Funcionalidade: Cadastro Online
	Eu, como cliente, desejo realizar cadastro online
	Atraves do preenchimento do formulario com os dados pessoais
	E os dados serem salvos com sucesso no banco de dados.

@register
Cenario: Realizar cadastro online
	Dado que o cliente acesse o site
	E preencha o formulario
	Quando clicar no botao Submit
	Entao sera apresentado uma mensagem de sucesso


	