package core;

import java.util.List;

import javax.swing.Action;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import enums.ByValue;

public class BasePage {
	private WebDriver driver;
	private ByValue byValue;
	private String value;

	public BasePage(WebDriver driver, ByValue byValue, String value) {
		this.driver = driver;
		this.byValue = byValue;
		this.value = value;
	}

	public WebElement elementoClicavel(WebDriver driver) {
		try {
			Thread.sleep(20);
			WebDriverWait wait = new WebDriverWait(driver, 20);
			switch (byValue) {
			case XPATH:
				return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(value)));
			case ID:
				return wait.until(ExpectedConditions.presenceOfElementLocated(By.id(value)));

			case CLASS_NAME:
				return wait.until(ExpectedConditions.presenceOfElementLocated(By.className(value)));

			case CSS:
				return wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(value)));
			default:
				return null;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void clicar(WebDriver driver) {
		elementoClicavel(driver).click();
	}

	public void escrever(WebDriver driver, String string) {
		elementoClicavel(driver).sendKeys(string);
	}

	public void selecionarCombo(WebDriver driver, String visibleText) {
		Select select = new Select(elementoClicavel(driver));
		select.selectByVisibleText(visibleText);
	}

	public String obterTexto(WebDriver driver) {
		WebElement text = elementoClicavel(driver);
		return text.getText();
	}

	public void scroll(WebDriver driver) throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments [0] .scrollIntoView ();", elementoClicavel(driver));
	}

	public void selecionarOpcao(WebDriver driver, WebElement[]...opcaoSelect) throws InterruptedException {
		WebElement countryList = elementoClicavel(driver);
		Select combo = new Select(countryList);
		List<WebElement> allSelectedOptions = combo.getAllSelectedOptions();
		for (WebElement country : allSelectedOptions) {
			if (country.getText().equals(opcaoSelect)) {
				country.click();
				Thread.sleep(3000);
				break;
			}
		}
	}
	
	public void clicarNoPais(WebDriver driver, String opcaoSelect) {
		WebElement select = elementoClicavel(driver);
		select.click(); // clico para mostrar as op��es

		WebElement lista = driver.findElement(By.id("select2-country-results"));
		List<WebElement> opcoes = lista.findElements(By.cssSelector("li"));

		for(WebElement opcao : opcoes) {
			if(opcao.getText().contains(opcaoSelect)) {
				opcao.click();
				break;
			}
		}
	}

	public void moverMouse(WebDriver driver) throws InterruptedException {
		WebElement elemento = elementoClicavel(driver);
		Actions act = new Actions(driver);
		act.moveToElement(elemento).perform();
		Thread.sleep(1000);
		act.moveToElement(elemento).click().build().perform();
		Thread.sleep(1000);

	}
	
		
}