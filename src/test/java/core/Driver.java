package core;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Driver {
	static WebDriver driver;

	public Driver() throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.automationtesting.in/Register.html");
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
	}	

	public static WebDriver getDriver() {
		return driver;
	}

	public static void acessarURL(WebDriver driver, String url) {
		driver.get(url);
	}

}
