package steps;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import core.Driver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.RegisterPage;

public class RegisterStep {
	static WebDriver driver;
	
	RegisterPage page = new RegisterPage(driver);
	
	@Dado("^que o cliente acesse o site$")
	public void que_o_cliente_acesse_o_site() throws Throwable {
		new Driver();
		driver = Driver.getDriver();		
	}

	@Dado("^preencha o formulario$")
	public void preencha_o_formulario() throws Throwable {
//		page.registerOnline(driver);
//		page.mouse(driver);
		page.registerOnline(driver);
	}

	@Quando("^clicar no botao Submit$")
	public void clicar_no_botao_Submit() throws Throwable {
		page.clicarNoBotaoSubmit(driver);
	}

	@Entao("^sera apresentado uma mensagem de sucesso$")
	public void sera_apresentado_uma_mensagem_de_sucesso() throws Throwable {
		Assert.assertEquals("Register", page.pegarTituloPagina(driver));
	}
	
	
	
	@After(order = 1)
	public void screenshot(Scenario scenario) {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(file, new File("target\\screenshots\\" + scenario.getId() + ".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@After(order = 0)
	public void fecharBrowser() throws InterruptedException {
		driver.quit();
	}
	
	

}
