package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import core.BasePage;
import enums.ByValue;

public class RegisterPage {
	static WebDriver driver;

	public RegisterPage(WebDriver driver) {
	}

	public BasePage firstName = new BasePage(driver, ByValue.XPATH, ".//label[text()='Full Name* ']/following-sibling ::div[1]/input");
	public BasePage lastName = new BasePage(driver, ByValue.XPATH, ".//label[text()='Full Name* ']/following-sibling ::div[2]/input");
	public BasePage address = new BasePage(driver, ByValue.CSS, "#basicBootstrapForm > div:nth-child(2) > div > textarea");
	public BasePage emailAddress = new BasePage(driver, ByValue.XPATH, "//*[@id='eid']/input");
	public BasePage phone = new BasePage(driver, ByValue.CSS, "#basicBootstrapForm > div:nth-child(4) > div > input");
	public BasePage gender = new BasePage(driver, ByValue.XPATH, "//input[1][@name='radiooptions']");
	public BasePage hobbies = new BasePage(driver, ByValue.XPATH, "//input[@id='checkbox2']");
	public BasePage languages = new BasePage(driver, ByValue.XPATH, ".//label[text()='Languages']/following-sibling::div[1]/multi-select");
	public BasePage languageContry = new BasePage(driver, ByValue.XPATH, "//*[@id=\"basicBootstrapForm\"]/div[7]/div/multi-select/div[2]/ul/li[1]/a");
	public BasePage skills = new BasePage(driver, ByValue.XPATH, "//select[@id='Skills']");
	public BasePage country = new BasePage(driver, ByValue.XPATH, "//select[@id='countries']");
	public BasePage selectCountry = new BasePage(driver, ByValue.CSS, ".select2");
	public BasePage year = new BasePage(driver, ByValue.XPATH, "//select[@id='yearbox']");
	public BasePage month = new BasePage(driver, ByValue.XPATH, "//select[@placeholder='Month']");
	public BasePage day = new BasePage(driver, ByValue.XPATH, "//select[@id='daybox']");
	public BasePage password = new BasePage(driver, ByValue.XPATH, "//input[@id='firstpassword']");
	public BasePage confirmPassword = new BasePage(driver, ByValue.XPATH, "//input[@id='secondpassword']");
	public BasePage submit = new BasePage(driver, ByValue.XPATH, "//button[@id='submitbtn']");
	
	
	public void registerOnline(WebDriver driver) throws InterruptedException {
		firstName.escrever(driver, "Noah");
		lastName.escrever(driver, "Maximiano");
		address.escrever(driver, "AV JOS� J�LIO, 221");
		emailAddress.escrever(driver, "noahmax@agriness.com");
		phone.escrever(driver, "4075554321");
		gender.clicar(driver);
		hobbies.clicar(driver);
		languages.scroll(driver);
		languages.moverMouse(driver);
		languageContry.moverMouse(driver);		
		skills.selecionarCombo(driver, "Java");
		country.selecionarCombo(driver, "Brazil");
		selectCountry.clicarNoPais(driver, "India");
		year.selecionarCombo(driver, "2000");
		month.selecionarCombo(driver, "January");
		day.selecionarCombo(driver, "1");
		password.escrever(driver, "Max5105*");
		confirmPassword.escrever(driver, "Max5105*");
	}	
	
	public void clicarNoBotaoSubmit(WebDriver driver) {
		submit.clicar(driver);
	}
	
	public String pegarTituloPagina(WebDriver driver) {
		System.out.println("Como o bot�o Submit est� sem a��o, apenas validei o t�tulo da p�gina!");
		 String titulo = driver.getTitle();
		 return titulo;
	}


}
