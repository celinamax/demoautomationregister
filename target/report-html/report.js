$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/register_online.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "# language : pt"
    }
  ],
  "line": 3,
  "name": "Cadastro Online",
  "description": "Eu, como cliente, desejo realizar cadastro online\r\nAtraves do preenchimento do formulario com os dados pessoais\r\nE os dados serem salvos com sucesso no banco de dados.",
  "id": "cadastro-online",
  "keyword": "Funcionalidade"
});
formatter.scenario({
  "line": 9,
  "name": "Realizar cadastro online",
  "description": "",
  "id": "cadastro-online;realizar-cadastro-online",
  "type": "scenario",
  "keyword": "Cenario",
  "tags": [
    {
      "line": 8,
      "name": "@register"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "que o cliente acesse o site",
  "keyword": "Dado "
});
formatter.step({
  "line": 11,
  "name": "preencha o formulario",
  "keyword": "E "
});
formatter.step({
  "line": 12,
  "name": "clicar no botao Submit",
  "keyword": "Quando "
});
formatter.step({
  "line": 13,
  "name": "sera apresentado uma mensagem de sucesso",
  "keyword": "Entao "
});
formatter.match({
  "location": "RegisterStep.que_o_cliente_acesse_o_site()"
});
formatter.result({
  "duration": 36483248400,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStep.preencha_o_formulario()"
});
formatter.result({
  "duration": 8966599300,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStep.clicar_no_botao_Submit()"
});
formatter.result({
  "duration": 173581300,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStep.sera_apresentado_uma_mensagem_de_sucesso()"
});
formatter.result({
  "duration": 26616400,
  "status": "passed"
});
formatter.after({
  "duration": 450156400,
  "status": "passed"
});
formatter.after({
  "duration": 3503084000,
  "status": "passed"
});
});